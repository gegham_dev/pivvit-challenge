<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pivvit Challenge</title>
    </head>
    <body>
        <div id="app">
            <App />
        </div>
        <script  src="<?php echo e(asset('js/app.js')); ?>"></script>
    </body>
</html>
<?php /**PATH C:\xampp\htdocs\pivvit-challenge\resources\views/index.blade.php ENDPATH**/ ?>