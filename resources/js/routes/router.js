const VueRouter = require('vue-router').default
import Vue from 'vue'

import CreatePurchase from '../components/CreatePurchase'
import Purchases from '../components/Purchases'
import Login from '../components/auth/Login'
import Register from '../components/auth/Register'

const routes = [
    {path: '/', component: CreatePurchase, name: 'CreatePurchase'},
    {path: '/purchases', component: Purchases, name: 'Purchases'},
    {path: '/login', component: Login, name: 'Login'},
    {path: '/register', component: Register, name: 'Register'},
];

const router = new VueRouter({
    mode: 'history',
    routes,
});

Vue.use(VueRouter)

export default router