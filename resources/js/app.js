/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import router from './routes/router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// vue bootstrap
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

// auth middleware
// router.beforeEach((to, from, next) => {
// 	if(to.matched.some(record => record.meta.requiresAuth) && !store.getters.isAuthenticated) {
// 		next({name: 'login'})
// 		return
// 	}
// 	next()
// })

// vue-axios
Vue.use(VueAxios, axios)
axios.defaults.headers.common["Accept"] = "application/json"
axios.defaults.headers.common["Content-Type"] = "application/json"

// let accessToken = store.getters.accessToken
// if (accessToken) {
// 	axios.defaults.headers.common["Authorization"] = "Bearer " + accessToken;
// }

Vue.config.productionTip = false

Vue.component('app', require('./components/App.vue').default);

const app = new Vue({
    el: '#app',
    router,
});
