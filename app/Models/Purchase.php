<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = [
        'customerName',
        'offeringID',
        'quantity',
    ];

    public function offering() {
        return $this->belongsTo(Offering::class, 'offeringID');
    }
}
