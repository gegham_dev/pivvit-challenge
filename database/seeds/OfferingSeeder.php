<?php

use App\Models\Offering;
use Illuminate\Database\Seeder;

class OfferingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 10; $i++){
            Offering::create([
                'title' => "Offering $i",
                'price' => rand(100, 500),
                'quantity' => rand(10, 50),
            ]);
        }
    }
}
